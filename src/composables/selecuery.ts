
import init, { transpile_select_statement } from 'selecuery-wasm/pkg';
import { ref, unref, type Ref, isRef, watchEffect } from 'vue';

export function useSelecuery(selectStmtToTranspileToQuery: string | Ref<string>) {

    const selectStmtToTranspile = ref('');
    const transpiledSelectStmt = ref('');

    function doTranspile() {

        selectStmtToTranspile.value = unref(selectStmtToTranspileToQuery);

        init().then(() => {
            transpiledSelectStmt.value = transpile_select_statement(selectStmtToTranspile.value);
        });
    }

    if (isRef(selectStmtToTranspileToQuery)) {
        watchEffect(doTranspile);
    } else {
        doTranspile();
    }

    return { transpiledSelectStmt, selectStmtToTranspile }
}
