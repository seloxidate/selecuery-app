use dracaena_root::root::*;
use std::error::Error;

#[test]
fn test_this() -> Result<(), Box<dyn Error>> {
    let select_stmt = r#"select firstonly custTable
                        where custTable.AccountNum == '1234';"#;

    let ast_root: AstRootNode = select_stmt
        .parse()
        .map_err(|err| format!("Parsing failed: {}", err))?;

    Ok(())
}
