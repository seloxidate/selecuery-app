mod utils;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub fn transpile_select_statement(select_stmt: &str) -> String {
    use dracaena_root::root::*;

    let ast_root: Result<AstRootNode, String> = select_stmt
        .parse()
        .map_err(|err| format!("Parsing failed: {}", err));

    match ast_root {
        Ok(ast_root) => match ast_root.transpile_to_query().map(|ast| ast.code_fmt()) {
            Ok(s) | Err(s) => s,
        },
        Err(err_str) => err_str,
    }
}
